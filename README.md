# Facebook Clone Application

## Testing on Expo

### Requirement

For android user:

- Install Airdroid Cast on mobile phone to link to laptop device
- Install Expo Go on mobile phone to test react-native app

## Steps

- Clone this repository
- Install all dependencies:

```
yarn install
```

- Run react native app:

```
yarn start
```

- There will be a QR code present on Terminal. Open your Expo Go app on your phone and scan this QR code. Your app will be built and you will see the result. Good luck
