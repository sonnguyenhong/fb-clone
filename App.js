
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Login from './src/pages/Login';
import Introduce from './src/pages/Register/Introduce';
import NameScreen from './src/pages/Register/NameScreen';
import BirthDayScreen from './src/pages/Register/BirthdayScreen';
import TermsandPrivacy from './src/pages/Register/TermsandPrivacy';
import GenderScreen from './src/pages/Register/GenderScreen';
import PhoneScreen from './src/pages/Register/PhoneScreen';
import PasswordScreen from './src/pages/Register/PasswordScreen';

const Stack = createNativeStackNavigator();
export default function App() {
  return (
    // <View>
    //     {/* <Header></Header>
    //     <Navigation /> */}
    //     {/* <Login /> */}
    // </View>
    <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
                <Stack.Screen name='Create account' component={Introduce} />
                <Stack.Screen name='Name' component={NameScreen} />
                <Stack.Screen name='Birthday' component={BirthDayScreen} />
                <Stack.Screen name='Gender' component={GenderScreen} />
                <Stack.Screen name='Mobile number' component={PhoneScreen} />
                <Stack.Screen name='Password' component={PasswordScreen} />
                <Stack.Screen name='Terms & Privacy' component={TermsandPrivacy} />
            </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
