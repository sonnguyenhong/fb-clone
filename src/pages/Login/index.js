import React from 'react';
import { View, Text, Image, StatusBar, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { TextInput } from "@react-native-material/core";

const Login = ({ navigation }) => {
    return (
        <View>
            <StatusBar
                barStyle={'light-content'}
                translucent={true}
                backgroundColor={'rgba(0,0,0,0)'}
            />

            <Image
                source={
                    require('../../../assets/loginbanner.png')
                }
                style={styles.banner}
            />
            <View style={styles.inputcontainer}>
                <TextInput
                    label="Phone or email"
                    variant="standard"
                    style={styles.input}
                />
                <TextInput
                    label="Password"
                    variant="standard"
                    style={styles.input}
                    secureTextEntry={true}
                />
            </View>
            <TouchableOpacity style={styles.Logincontainer} >
                <Text style={styles.Logintext}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.Passwordcontainer}>
                <Text style={styles.Passwordtext}>Forgot Password?</Text>
            </TouchableOpacity>

            <View style={styles.Orcontainer}>
                <View style={styles.çizgi}></View>
                <Text>OR</Text>
                <View style={styles.çizgi}></View>
            </View>

            <TouchableOpacity style={styles.Sigincontainer}
                onPress={() => navigation.navigate('Create account')}
            >
                <Text style={styles.Sigintext}>Create new Facebook account</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Login;

const styles = StyleSheet.create({
    banner: {
        width: Dimensions.get('window').width,
    },
    dilcontainer: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    moretext: {
        color: '#1877F2'
    },
    inputcontainer: {
        marginTop: 50
    },
    input: {
        // borderBottomWidth: 2,
        borderColor: '#E4E6EB',
        marginHorizontal: 35,
        fontSize: 17,
        marginTop: 10
    },
    Logincontainer: {
        backgroundColor: '#1877F2',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 35,
        borderRadius: 8,
        marginVertical: 20
    },
    Logintext: {
        fontWeight: 'bold',
        color: '#fff',
        paddingVertical: 10,
        fontSize: 18
    },
    Passwordcontainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    Passwordtext: {
        fontWeight: 'bold',
        color: '#1877F2',
        fontSize: 18
    },
    Orcontainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 45
    },
    çizgi: {
        borderColor: '#E4E6EB',
        borderTopWidth: 3,
        width: 150,
        alignSelf: 'center',
        marginHorizontal: 5
    },
    Sigincontainer: {
        backgroundColor: '#2FA24B',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 60,
        borderRadius: 8,
    },
    Sigintext: {
        fontWeight: 'bold',
        color: '#fff',
        paddingVertical: 10,
        fontSize: 15,
    }
})