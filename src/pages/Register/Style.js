import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    ButtonContainer: {
        backgroundColor: '#1877F2',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 35,
        borderRadius: 8,
        marginVertical: 20
    },
    ButtonText: {
        fontWeight: 'bold',
        color: '#fff',
        paddingVertical: 10,
        fontSize: 18
    },
    ContentContainer: {
        textAlign: 'center',
        marginVertical: 20,
        marginHorizontal: 35,
    },
    Title: {
        fontWeight: 'bold',
        fontSize:18
    },
    Banner: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 2,
        resizeMode: 'contain'
    },
    InputContainer: {
        flexDirection: "row",
        marginHorizontal: 35,
    },
    InputText: {
        flex:1
    },
    ButtonPhoneBookcontainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 35,
        borderRadius: 8,
        marginVertical: 20
    },
    ButtonPhoneBookText: {
        color: '#1877F2',
        fontSize: 13
    },
    DateInput: {
    
        marginHorizontal: 35,
    }
})

export {styles}