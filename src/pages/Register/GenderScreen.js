import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { RadioButton } from "react-native-paper"
import { styles } from "./Style";

export default function GenderScreen({ navigation }) {
    const [value, setValue] = React.useState('male');

    return (
        <View>
            <Text style={styles.ContentContainer}>
                <Text style={styles.Title}>
                    What's your gender?
                    {"\n\n"}
                </Text>
                <Text>
                    You can change who sees your gender on your profile later
                </Text>
            </Text>

            <RadioButton.Group onValueChange={newValue => setValue(newValue)} value={value}>
                <RadioButton.Item label="Male" value="male" />
                <RadioButton.Item label="Female" value="female" />
            </RadioButton.Group>

            <TouchableOpacity style={styles.ButtonContainer}
                onPress={() => navigation.navigate('Mobile number')}
            >
                <Text style={styles.ButtonText}>
                    Next
                </Text>
            </TouchableOpacity>
        </View>
    );
}