import { Text, View, TouchableOpacity } from "react-native";
import { TextInput } from "@react-native-material/core";
import { styles } from "./Style";

export default function PasswordScreen({ navigation }) {

    return (
        <View>
            <Text style={styles.ContentContainer}>
                <Text style={styles.Title}>
                    Choose a password
                    {"\n\n"}
                </Text>
                <Text>
                    Create a password with at least 6 characters. It should be something others couldn't guess
                </Text>

            </Text>

            <View style={styles.InputContainer}>
                <TextInput
                    label="Password"
                    variant="standard"
                    style={styles.InputText}
                />
            </View>


            <TouchableOpacity style={styles.ButtonContainer}
                onPress={() => navigation.navigate('Terms & Privacy')}
            >
                <Text style={styles.ButtonText} >
                    Next
                </Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.ButtonPhoneBookcontainer}>
                <Text style={styles.ButtonPhoneBookText}>Đăng kí bằng địa chỉ email</Text>
            </TouchableOpacity> */}
        </View>
    );
}