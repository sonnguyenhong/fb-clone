import React, { useState } from 'react';
import moment from 'moment'
import { Text, View, TouchableOpacity } from "react-native";
import { styles } from "./Style";
// import DatePicker from 'react-native-date-picker'
import DateTimePicker from '@react-native-community/datetimepicker';
import { Button } from 'react-native-paper';

export default function BirthDayScreen({ navigation }) {
    const [date, setDate] = useState(new Date())

    return (
        <View>
            <Text style={styles.ContentContainer}>
                <Text style={styles.Title}>
                    What's your birthday?
                    {"\n\n"}
                </Text>
                <Text>
                    Choose your date of birth. You can always make this private later
                </Text>
            </Text>
            <Text style={{textAlign: 'center', fontSize: 24}}>{moment(date).format('DD-MM-YYYY')}</Text>

            {/* <DatePicker style={styles.DateInput}
                androidVariant='nativeAndroid'
                mode="date"
                locale='en'
                format="DD/MM/YYYY"
                maximumDate={date}
                date={date}
                textColor="#000000"

                onDateChange={setDate}
            /> */}

            <DateTimePicker value={date} mode='date'/>

            <TouchableOpacity style={styles.ButtonContainer}
                onPress={() => navigation.navigate('Gender')}
            >
                <Text style={styles.ButtonText}>
                    Next
                </Text>
            </TouchableOpacity>
        </View>
    );
}