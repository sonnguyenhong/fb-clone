import { Text, View, TouchableOpacity } from "react-native";
import { TextInput } from "@react-native-material/core";
import { styles } from "./Style";

export default function PhoneScreen({ navigation }) {

    return (
        <View>
            <Text style={styles.ContentContainer}>
                <Text style={styles.Title}>
                    Enter your mobile number
                    {"\n\n"}
                </Text>
                <Text>
                    Enter the mobile number where you can be reached. You can hide this from your profile later
                </Text>

            </Text>

            <View style={styles.InputContainer}>
                <TextInput
                    label="Mobile number"
                    variant="standard"
                    style={styles.InputText}
                />
            </View>


            <TouchableOpacity style={styles.ButtonContainer}
                onPress={() => navigation.navigate('Password')}
            >
                <Text style={styles.ButtonText}>
                    Next
                </Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.ButtonPhoneBookcontainer}>
                <Text style={styles.ButtonPhoneBookText}>Đăng kí bằng địa chỉ email</Text>
            </TouchableOpacity> */}
        </View>
    );
}