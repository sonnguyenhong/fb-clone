import { View, Text, TouchableOpacity, Image } from "react-native"
import { styles } from "./Style";

// giao diện tạo tài khoản
export default function Introduce({ navigation }) {
    return (
        <View>
            <Image source={require("../../../assets/Blog-post-pana.png")} style={styles.Banner} />

            <Text style={styles.ContentContainer}>
                <Text style={styles.Title}>
                    Join FaceBook
                    {"\n\n"}
                </Text>
                <Text>
                    We'll help you create a new account in a few easy steps
                </Text>
            </Text>

            <TouchableOpacity style={styles.ButtonContainer}
                onPress={() => navigation.navigate('Name')}
            >
                <Text style={styles.ButtonText}>
                    Next
                </Text>
            </TouchableOpacity>
        </View>
    );
}
