import { Text, TouchableOpacity, View } from "react-native";
import { styles } from "./Style";

export default function TermsandPrivacy({ navigation }) {
    return (
        <View>
            <Text style={styles.ContentContainer}>
                <Text style={styles.Title}>
                    Finish signing up
                    {"\n\n"}
                </Text>
                <Text>
                    People who use our service many have uploaded your contect information to Facebook. Learn more
                    {"\n\n"}
                </Text>
                <Text>
                    By tapping Sign up, you agree to you Terms, Data Policy and Cookies Policy. You may receive SMS notifications from us and can opt out any time.
                </Text>
            </Text>

            <TouchableOpacity style={styles.ButtonContainer}>
                <Text style={styles.ButtonText}>
                    Sign up
                </Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.ButtonPhoneBookcontainer}>
                <Text style={styles.ButtonPhoneBookText}>Đăng kí mà không tải danh bạ của tôi lên</Text>
            </TouchableOpacity> */}
        </View>
    );
}