import { View, Text, TouchableOpacity } from "react-native"
import { TextInput } from "@react-native-material/core";
import { styles } from "./Style";

//giao diện điền tên
export default function NameScreen({ navigation }) {
    return (
        <View>
            <Text style={styles.ContentContainer}>
                <Text style={styles.Title}>
                    What's your name?
                    {"\n\n"}
                </Text>
                <Text>
                    Enter the name you see in real life
                </Text>
            </Text>

            <View style={styles.InputContainer}>
                <TextInput
                    label="First Name"
                    variant="standard"
                    style={styles.InputText}
                />

                <TextInput
                    label="Last Name"
                    variant="standard"
                    style={styles.InputText}
                />
            </View>


            <TouchableOpacity style={styles.ButtonContainer}
                onPress={() => navigation.navigate('Birthday')}
            >
                <Text style={styles.ButtonText}>
                    Next
                </Text>
            </TouchableOpacity>
        </View>
    );
}