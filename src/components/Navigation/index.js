import { View, StyleSheet } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHouse, faUserGroup, faUsers, faStore, faBell, faBars } from '@fortawesome/free-solid-svg-icons';
import {} from 'react-native-vector-icons/AntDesign';
import {AiOutlineHome} from 'react-icons/ai';

function Navigation() {
    const styles = StyleSheet.create({
        wrapper: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        iconWrapper: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 8,
            paddingRight: 8,
            flex: 1
        },
        icon: {
            color: '#555',
        }
    });

    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <View style={styles.iconWrapper}>
                    {/* <i class="fa-regular fa-house"></i> */}
                    <FontAwesomeIcon style={styles.icon} icon={faHouse} size={24} />
                </View>
                <View style={styles.iconWrapper}>
                    <FontAwesomeIcon style={styles.icon} icon={faUserGroup} size={24} />
                </View>
                <View style={styles.iconWrapper}>
                    <FontAwesomeIcon style={styles.icon} icon={faUsers} size={24} />
                </View>
                <View style={styles.iconWrapper}>
                    <FontAwesomeIcon style={styles.icon} icon={faStore} size={24} />
                </View>
                <View style={styles.iconWrapper}>
                    <FontAwesomeIcon style={styles.icon} icon={faBell} size={24} />
                </View>
                <View style={styles.iconWrapper}>
                    <FontAwesomeIcon style={styles.icon} icon={faBars} size={24} />
                </View>
            </View>
        </View>
    );
}

export default Navigation;
