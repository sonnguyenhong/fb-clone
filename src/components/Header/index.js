import { View, Image, StyleSheet } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import { faFacebookMessenger } from '@fortawesome/free-brands-svg-icons';

function Header() {
    const marginTop = 20;
    const styles = StyleSheet.create({
        container: {},
        wrapper: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        icon: {
            display: 'flex',
            flexDirection: 'row',
            marginRight: 16,
        },
        facebookLogo: {
            width: 150,
            height: 60,
            marginLeft: 8,
            marginTop: marginTop,
        },
        searchIconWrapper: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 8,
            paddingRight: 8,
            marginTop: marginTop + 10,
            borderRadius: 999,
            alignSelf: 'baseline',
            marginRight: 8,
            backgroundColor: '#eee',
        },
        searchIcon: {},
        messengerIconWrapper: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 8,
            paddingRight: 8,
            marginTop: marginTop,
            borderRadius: 999,
            alignSelf: 'baseline',
            backgroundColor: '#eee',
        },
        messengerIcon: {},
    });

    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <Image style={styles.facebookLogo} source={require('../../../assets/logo/facebook-logo.png')} />
                <View style={styles.icon}>
                    <View style={styles.searchIconWrapper}>
                        <FontAwesomeIcon style={styles.searchIcon} icon={faMagnifyingGlass} size={24} />
                    </View>
                    <View style={styles.messengerIconWrapper}>
                        <FontAwesomeIcon style={styles.messengerIcon} icon={faFacebookMessenger} size={24} />
                    </View>
                </View>
            </View>
        </View>
    );
}

export default Header;
